<?php
/**
 * Site header template
 *
 * @package Blog Theme
 * @since 1.0
 * @author Dornaweb
 */

add_action('after_setup_theme', 'dw_setup_my_blog_theme');
function dw_setup_my_blog_theme() {
    /**
     * Auto <title> tags
     */
    add_theme_support('title-tag');
}

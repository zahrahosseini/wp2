<?php
/**
 * Site Footer template
 *
 * @package Blog Theme
 * @since 1.0
 * @author Dornaweb
 */ ?>

<footer class="siteFooter">
    <div class="container">
        <i class="siteFooter__copy icon-ivahid"></i>
        <p class="siteFooter__copyContent">اين وبسايت متعلق به من ( مهسا جعفری عشقول D: ) ميباشد و تمامی حقوق آن
            محفوظ ميباشد .
        </p>
        <div class="siteFooter__social">
            <i class="icon-facebook"></i>
            <i class="icon-googleP"></i>
            <i class="icon-twitter"></i>
            <i class="icon-insta"></i>
            <i class="icon-telegram"></i>
        </div>
    </div>

</footer>

<?php wp_footer(); ?>
</body>
</html>

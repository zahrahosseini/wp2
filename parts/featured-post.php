<?php
/**
 * Blog post item
 * Used in WP_Query loops
 *
 * @package Blog Theme
 * @since 1.0
 * @author Dornaweb
 */ ?>

<article class="siteBranding">
    <figure class="siteBranding__cover">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/sample/slide1.jpg" alt="">
    </figure>
    <header class="siteBranding__header">
        <a href="" class="uiTag">
            <i class="icon-sharp"></i>
            <span>برندینگ</span>
        </a>
        <h1 class="siteBranding__headerTitle">
            <a href="">بررسی افزونه های نظرات محصول
                ووکامرس برای قسمت اول</a>
        </h1>
    </header>
    <p class="siteBranding__content">امروز مطلبی تحت عنوان بررسی افزونه های
        نظرات محصول ووکامرس برای وردپرس در
        دو قسمت براتون آماده کردیم .اگر شما
        دوست دارید از افزونه هایی استفاده کنید
        که از لحاظ سئو برای وب سایت ...</p>
    <div class="sitePost__coverDetails">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/sample/starGreen.png" alt="">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/sample/starGreen.png" alt="">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/sample/starGreen.png" alt="">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/sample/starGreen.png" alt="">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/sample/starGray.png" alt="">
    </div>
</article>

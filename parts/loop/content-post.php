<article class="sitePosts__item">
    <img class="sitePosts__itemCover" src="<?php echo get_template_directory_uri(); ?>/assets/img/sample/article.jpg" alt="">
    <div class="sitePosts__itemContent">
        <h1 class="sitePosts__itemContent__title">
            <a href="">
                <?php the_title(); ?>
            </a>
        </h1>
        <a href="" class="uiTag">
            <i class="icon-sharp"></i>
            <span>برندینگ</span>
        </a>
        <div class="sitePosts__itemContent__description">
            <p>
                امروز مطلبی تحت عنوان بررسی افزونه های
                نظرات محصول ووکامرس برای وردپرس در
                دو قسمت براتون آماده کردیم .اگر شما
                دوست دارید از افزونه هایی استفاده کنید
                که از لحاظ سئو برای وب سایت ...
            </p>
            <a class="sitePosts__itemContent__descriptionMore" href="<?php the_permalink(); ?>">بخوانید</a>
        </div>
    </div>
</article>

<?php
/**
 * Homepage
 *
 * @package Blog Theme
 * @since 1.0
 * @author Dornaweb
 */

get_header();
?>
<main>
    <div class="container">
        <?php get_template_part('parts/featured-post'); ?>

        <div class="sitePosts">
            <div class="row mar13">
                <?php while (have_posts()) : the_post(); ?>
                    <div class="col-md-8">
                        <?php get_template_part('parts/loop/content-post'); ?>
                    </div>
                <?php endwhile; wp_reset_postdata(); ?>

                <div class="col-md-8">
                    <div class="sitePosts__advertise">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/sample/learn.png" alt="">
                        <a href="">مشاهده کانال آموزشی آی وحید
                            <span class="sitePosts__advertiseChannel">
                                <i class="icon-arrowB"></i>
                            </span>
                        </a>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="sitePosts__paragraph">

                        <a href="">از کنسول جستجوی گوگل برای پیدا کردن
                            مشکلات تگ ها استفاده کنید	</a>
                    <p>گوگل صراحتا اعلام کرده که عناوین صفحات می بایست به صورت مستقل و غیر تکراری نوشته شوند. با استفاده از کنسول گوگل می توانید صفحاتی که احیانا برای آنها عنوانی ننوشته یا عناوین تکراری برای صفحات گذاشته اید را پیدا شوند. با استفاده از کنسول گوگل می توانید شوند. با استفاده از کنسول گوگل می توانید کنید.</p>

                    </div>
                </div>
            </div>

        </div>
        <ul class="sitePagination">
            <li>
                <a href="#" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                </a>
            </li>
            <li><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li class="sitePagination__select"><a href="#">5</a></li>
            <li>
                <a href="#" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                </a>
            </li>
        </ul>
    </div>
</main>
<?php
get_footer();

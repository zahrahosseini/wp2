<?php
/**
 * Site header template
 *
 * @package Blog Theme
 * @since 1.0
 * @author Dornaweb
 */ ?><!DOCTYPE html>
<html class="no-js" lang="fa-IR" dir="rtl">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
	<meta name="google" content="notranslate" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="designer" content="ivahid.com" />
	<meta name="copyright" content="&copy; 2016 ivahid.com" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<meta name="theme-color" content="کد رنگ اصلی قالب" />
	<meta name="msapplication-navbutton-color" content="کد رنگ اصلی قالب" />
	<meta name="apple-mobile-web-app-status-bar-style" content="کد رنگ اصلی قالب" />
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/stylesheet.css" />
	<!--[if lt IE 9]>
      <script src="<?php echo get_template_directory_uri(); ?>/assets/js/html5.js"></script>
    <![endif]-->

    <?php wp_head(); ?>
</head>

<body>
	<header class="siteHeader">
		<div class="siteHeader__top">
			<div class="container">
				<nav class="siteHeader__topMenu">
					<ul>
						<li><a href="">جدیدترین نوشته‌ها</a></li>
						<li><a href="">نوشته‌های دوستان</a></li>
						<li><a href="">استارتاپ کسب و کار</a></li>
						<li><a href="">خلاقیت برنامه نویسی</a></li>
						<li><a href="">طنز</a></li>
						<li><a href="">روانشناسی</a></li>
					</ul>
				</nav>
				<img class="siteHeader__logo" src="<?php echo get_template_directory_uri(); ?>/assets/img/sample/logo.png" alt="لوگو">
			</div>
		</div>
		<div class="siteHeader__middle">
			<div class="container">
				<ul class="siteHeader__middleMenu">
					<li><a href="">همه</a></li>
					<li><a href="">آموزش ها</a></li>
					<li><a href="">اسکریپت ها</a></li>
					<li><a href="">افزونه ها</a></li>
					<li><a href="">قالب</a></li>
				</ul>
			</div>
		</div>
		<div class="siteHeader__button">
			<div class="container">

				<input type="text" placeholder=" ... جستجو مطالب">
			</div>
		</div>
	</header>
